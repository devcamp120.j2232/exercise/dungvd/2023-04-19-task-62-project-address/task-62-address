package com.devcamp.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.location.model.Ward;

public interface IWardRepository extends JpaRepository<Ward, Long>{
    
}
