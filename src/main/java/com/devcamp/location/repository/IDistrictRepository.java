package com.devcamp.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.location.model.District;

public interface IDistrictRepository extends JpaRepository<District, Long>{
    // Hàm để tìm quận huyện theo Id;
    District findById(int id);
}
