package com.devcamp.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.location.model.Province;

public interface IProvinceRepository extends JpaRepository<Province, Long>{
    // Hàm để tìm tỉnh thành phố qua Id
    Province findById(int id);
}
