package com.devcamp.location.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.location.model.District;
import com.devcamp.location.model.Province;
import com.devcamp.location.repository.IDistrictRepository;
import com.devcamp.location.repository.IProvinceRepository;


@RestController
@CrossOrigin
public class DistrictController {
    @Autowired
    IProvinceRepository pIProvinceRepository;

    @Autowired
    IDistrictRepository pIDistrictRepository;

    @GetMapping("province/{id}")
    public ResponseEntity<Set<District>> getDistrictOfCity(@PathVariable int id) {
        try {
            Province vProvince = pIProvinceRepository.findById(id);
            if(vProvince!= null) {
                return new ResponseEntity<>(vProvince.getDistricts(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
 