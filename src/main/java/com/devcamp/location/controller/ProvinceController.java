package com.devcamp.location.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.location.model.Province;
import com.devcamp.location.repository.IProvinceRepository;


@RestController
@CrossOrigin
public class ProvinceController {
    @Autowired
    IProvinceRepository pIProvinceRepository;

    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getAllProvinces() {
        try {
            List<Province> listProvinces = new ArrayList<Province>();
            pIProvinceRepository.findAll().forEach(listProvinces::add);
            return new ResponseEntity<>(listProvinces, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces5")
    public  ResponseEntity<List<Province>> displayProvinceByPageSize(@RequestParam(name = "page", defaultValue = "1") String page, @RequestParam(name = "size",defaultValue =  "5") String size) {
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Province> listProvinces = new ArrayList<Province>();
            pIProvinceRepository.findAll(pageWithElements).forEach(listProvinces::add);
            return new ResponseEntity<>(listProvinces, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    }

