package com.devcamp.location.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.location.model.District;
import com.devcamp.location.model.Ward;
import com.devcamp.location.repository.IDistrictRepository;
import com.devcamp.location.repository.IWardRepository;

@RestController
@CrossOrigin
public class WardController {
    @Autowired
    IDistrictRepository pIDistrictRepository;

    @Autowired 
    IWardRepository pIWardRepository;


    // API trả về danh sách xã, phường của 1 quận huyện
    @GetMapping("/district")
    public ResponseEntity<Set<Ward>> getWardByDistrictId(@RequestParam(name = "id", required = true) int id) {
        
        try {
            // Lấy giá trị của Quận huyện thông qua ID truyền vào
            District vDistrict = pIDistrictRepository.findById(id);
        
            // Kiểm tra là có quận huyện nào quá ID đó không
            if(vDistrict != null) {
                //nếu có trả về Danh sách Phường, xã thông qua hàm GetWards, và status ok;
                return new ResponseEntity<>(vDistrict.getWards(), HttpStatus.OK);
            }
            else {
                // Nếu không trả về null và status not found
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
            // Nếu gặp lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
}
